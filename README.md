CSPP_MQTT extends the CSPP\_Core package of the CSPP\_-Project. 

It contains derived classes of

- CSPP_PVConnection
- CSPP_PVMonitor

Refer to https://git.gsi.de/EE-LV/CSPP/CSPP/-/wikis/home for CS++ project overview, details and documentation.

LabVIEW 2019 is the currently used development environment.

Related documents and information
=================================

- README.md
- EUPL v.1.1 - Lizenz.pdf
- Contact: H.Brand@gsi.de or D.Neidherr@gsi.de
- Download, bug reports... : https://git.gsi.de/EE-LV/CSPP/CSPP_MQTT.git
- Documentation:
  - Refer to package folder
  - NI Actor Framework: https://decibel.ni.com/content/groups/actor-framework-2011?view=overview

GIT Submodules
==============
This package can be used as submodule
- Packages\CSPP_MQTT:
  - CSPP_MQTTConnection
  - CSPP_MQTTMonitor

External Dependencies
---------------------

- CSPP_Core: http://github.com/HB-GSI/CSPP_Core
- LVMQTT: https://github.com/DAQIO/LVMQTT.git

Getting started:
=================================
- Add CSPP_MQTTContent.vi into your own LabVIEW project and
- Add into your desired case of the CSPP_UserContents.vi
- Move lvlib's from dependencies to a virtual folder in your project.
- You need to extend your project specific ini-file.
  - A sample ini-file should be available on disk in the corresponding package folder.
- You need to launch a MQTT request broker somewhere.
  - Mosquitto is an open source MQTT broker, https://mosquitto.org
		○ A test broker is available, https://test.mosquitto.org



Author: H.Brand@gsi.de, D.Neidherr@gsi.de

Copyright 2020  GSI Helmholtzzentrum für Schwerionenforschung GmbH

Planckstr.1, 64291 Darmstadt, Germany

Lizenziert unter der EUPL, Version 1.1 oder - sobald diese von der Europäischen Kommission genehmigt wurden - Folgeversionen der EUPL ("Lizenz"); Sie dürfen dieses Werk ausschließlich gemäß dieser Lizenz nutzen.

Eine Kopie der Lizenz finden Sie hier: http://www.osor.eu/eupl

Sofern nicht durch anwendbare Rechtsvorschriften gefordert oder in schriftlicher Form vereinbart, wird die unter der Lizenz verbreitete Software "so wie sie ist", OHNE JEGLICHE GEWÄHRLEISTUNG ODER BEDINGUNGEN - ausdrücklich oder stillschweigend - verbreitet.

Die sprachspezifischen Genehmigungen und Beschränkungen unter der Lizenz sind dem Lizenztext zu entnehmen.